#!/usr/bin/env bash


script_dir=$(dirname "$0")
backup_loc="$script_dir/saves/"
save_loc="/home/arcorion/.steam/steam/steamapps/compatdata/377840/pfx/drive_c/users/steamuser/AppData/LocalLow/SquareEnix/FINAL FANTASY IX/Steam/EncryptedSavedData/SavedData_ww.dat"

if ! [ -d "$backup_loc" ]
then
    mkdir -p "$backup_loc"
fi

if ! [ -f "$save_loc" ]
then
    echo "Save location for game is invalid. Quitting."
    exit
fi

backup_game () {
	backup_dir="$backup_loc$(date "+%d-%b-%Y_%H-%M")-$1/"
	while [ -d "$backup_dir" ]
	do
		sleep 1
		backup_dir="$backup_loc$(date "+%d-%b-%Y_%H-%M")-$1/"
	done
	mkdir -p "$backup_dir"
	echo "Backing up \""$save_loc"\" to \""$backup_dir"\""
	cp "$save_loc" "$backup_dir"
}

run_ffix () {
	steam -silent -applaunch 377840 2>&1 | tee >(
		while read line; do
			if [[ $line == *"Uploaded AppInterfaceStats to Steam"* ]]
			then
				sleep 5 
				steam -shutdown
				break
			fi
		done
)
}

latest_before="$backup_loc""$(ls -Art "$backup_loc" | grep before | tail -n 1)"/SavedData_ww.dat
read -r -p "Restore the most recent \"before\" game? (Y/N): " answer
if [ -f "$latest_before" ] 
then
	case $answer in
		[Yy]* )
			echo "Restoring game from \""$latest_before"\" to \""$save_loc"\""
			cp "$latest_before" "$save_loc"
			sleep 2
			;;
		[Nn]* )
			echo "Continuing on..."
			sleep 2
			;;
		* )
			echo "Not a valid response. Quitting."
			exit
			;;
	esac
fi

backup_game "before"
run_ffix
backup_game "after"
