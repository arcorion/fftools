#!/usr/bin/env bash

script_dir=$(dirname "$0")
game_id=$(xwininfo -tree -root | awk '/FINAL FANTASY IX/ { print $1 }')
log_dir="$script_dir/logs"
log_file="$log_dir/fftools.log"
image_dir="$script_dir/images-$(date +%m-%d-%Y)"

if ! [ -d $log_dir ]
then
    mkdir $log_dir
fi

if ! [ -d $image_dir ]
then
    mkdir $image_dir
fi

write_error () {
    time="$(date "+%m-%d-%Y %H:%M:%S")"
    echo $time - "$1" | tee -a "$log_file"
}

if xwd -out ./ff_image.xwd -id $game_id 2> /dev/null; then
    image_save="$image_dir/image-$(date +%H%M%S.%N).png"
    if convert ./ff_image.xwd "$image_save"
    then
        rm ./ff_image.xwd
        feh -x -g 640x480+1920+0 "$image_save" &
        sleep 1
        pkill feh
    else
        write_error "Error converting image."
    fi
else
	write_error "Failed to grab image. Is the game open?"
fi
